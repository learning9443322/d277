const contactForm = document.querySelector('#form');
const emailInput = document.querySelector('#email');
const confirmEmailInput = document.querySelector('#confirm_email');

contactForm.addEventListener('submit', (e) => {
  e.preventDefault();
  if (
    emailInput.value.toLowerCase() !== confirmEmailInput.value.toLowerCase()
  ) {
    return alert("The emails don't match!");
  }
  return alert('Form submitted!');
});
